.. pysword documentation master file, created by
   sphinx-quickstart on Wed Mar 21 14:25:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysword's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   README
   examples
   module-format
   pysword


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
